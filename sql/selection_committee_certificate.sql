-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: selection_committee
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `certificate`
--

DROP TABLE IF EXISTS `certificate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `certificate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ таблицы.',
  `subject_id` int(10) unsigned NOT NULL COMMENT 'Внешний ключ предмета, оценка за который хранится в таблице.',
  `user_id` int(10) unsigned NOT NULL COMMENT 'Внешний ключ пользователя, чья оценка хранится в таблице.',
  `subject_grade` int(2) unsigned NOT NULL COMMENT 'Оценка за экзамен по предмету.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `certificate_id_UNIQUE` (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `subject_id_idx` (`subject_id`),
  CONSTRAINT `subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Сертификат, в котором хранится оценка абитуриента за экзамен по определенному предмету. Проверяется и подтверждается администратором.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificate`
--

LOCK TABLES `certificate` WRITE;
/*!40000 ALTER TABLE `certificate` DISABLE KEYS */;
INSERT INTO `certificate` VALUES (1,1,1,95),(2,2,1,89),(3,3,1,76),(4,7,2,90),(5,8,2,87),(6,3,2,85),(7,7,4,89),(8,8,4,78),(9,3,4,83),(10,7,6,83),(11,8,6,91),(12,3,6,69),(13,7,13,71),(14,8,13,65),(15,3,13,59),(16,6,7,70),(17,1,7,80),(18,3,7,90),(145,6,41,56),(146,1,41,71),(147,3,41,39),(154,4,43,56),(155,6,43,67),(156,3,43,39),(157,2,44,59),(158,5,44,54),(159,3,44,89),(160,4,45,60),(161,6,45,87),(162,3,45,91),(163,4,46,80),(164,6,46,60),(165,3,46,95),(166,4,47,90),(167,6,47,77),(168,3,47,79),(169,4,48,56),(170,6,48,87),(171,3,48,39);
/*!40000 ALTER TABLE `certificate` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-23  3:27:04
