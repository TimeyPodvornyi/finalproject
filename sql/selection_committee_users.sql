-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: selection_committee
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ таблицы.',
  `login` varchar(45) NOT NULL COMMENT 'Логин, под которым пользователь зарегистрирован на сайте.',
  `password` blob NOT NULL COMMENT 'Пароль, который вводит пользователь для того, чтобы зайти на сайт.',
  `name` varchar(45) NOT NULL COMMENT 'Имя пользователя.',
  `middlename` varchar(45) NOT NULL COMMENT 'Фамилия пользователя.',
  `surname` varchar(45) NOT NULL COMMENT 'Отчество пользователя.',
  `email` varchar(45) NOT NULL COMMENT 'Адрес электонной почты, которую вводит пользователь при регистрации на сайте.',
  `phone` varchar(15) NOT NULL COMMENT 'Телефон, который вводит пользователь при регистрации на сайте.',
  `role_id` tinyint(3) unsigned NOT NULL COMMENT 'Внешний ключ к таблице user_role, которая хранит информацию о роли пользователя (абитуриент или администратор). ',
  `is_enlisted` tinyint(1) unsigned DEFAULT NULL COMMENT 'Поле, хранящее информацию о том, зачислен ли абитуриент на тот факультет, на который он подавал заявку. Может иметь значение NULL в случае, если заявка абитуриента еще не рассмотрена, или если пользователь является администратором.',
  `school_certificate` int(3) unsigned DEFAULT NULL COMMENT 'Средний балл аттестата абитуриента, округленный до одного знака после запятой и домноженный на 10. То есть он не содержит дробной части и может иметь значение от 0 до 100.',
  `faculty_abbr` char(3) DEFAULT NULL COMMENT 'Факультет, на котором зарегистрирован абитуриент.',
  `speciality_id` int(3) unsigned DEFAULT NULL COMMENT 'Специальность, на которую претендует абитуриент.',
  `is_appl_sent` tinyint(1) unsigned DEFAULT NULL COMMENT 'Поле, хранящее информацию о том, подал ли абитуриент заявку на поступление. Может иметь значение NULL в случае, если пользователь является администратором.',
  `is_appl_confirmed` tinyint(1) unsigned DEFAULT NULL COMMENT 'Поле, хранящее информацию о том, подтверждена ли заявка администратором. Может иметь значение NULL в случае, пользователь является администратором.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `role_id_idx` (`role_id`) /*!80000 INVISIBLE */,
  KEY `speciality_id_idx` (`speciality_id`),
  KEY `faculty_id_idx` (`faculty_abbr`),
  KEY `faculty_abbr_idx` (`faculty_abbr`),
  CONSTRAINT `faculty_abbr` FOREIGN KEY (`faculty_abbr`) REFERENCES `faculty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `speciality_id` FOREIGN KEY (`speciality_id`) REFERENCES `speciality` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Таблица, хранящая информацию о пользователе, который может быть как абитуриентом, так и администратором в зависимости от значения колонки role_id. ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'alex18',_binary '$2a$10$hNhz1zE39iHQCEi1qZa4r.OSX9GjbeIcOqHEDDFsaMJPVyfIpGUoW','Александр','Петрович','Одеялов','odalex18@gmail.com','29 4930914',2,1,87,'INF',14,1,1),(2,'ophelia',_binary '$2a$10$hNhz1zE39iHQCEi1qZa4r.OSX9GjbeIcOqHEDDFsaMJPVyfIpGUoW','Офелия','Альбертовна','Гильбертова','opheliag@gmail.com','33 3289001',2,1,91,'PHL',3,1,1),(3,'admin',_binary '$2a$10$yhZCPq/ZUinfoxMX9HYYruhtmOT1AwWLXlZdl6fJ21BR5Et55S8Be','Админов','Админ','Админович','admin@gmail.com','33 7600912',1,NULL,NULL,NULL,NULL,NULL,NULL),(4,'evlampia',_binary '$2a$10$hNhz1zE39iHQCEi1qZa4r.OSX9GjbeIcOqHEDDFsaMJPVyfIpGUoW','Евлампия','Прокофьевна','Аладушкина','evlampia30@gmail.com','25 9089370',2,1,98,'PHL',1,1,1),(5,'user',_binary '$2a$10$hNhz1zE39iHQCEi1qZa4r.OSX9GjbeIcOqHEDDFsaMJPVyfIpGUoW','Юзер','Юзерович','Ламер','user@mail.ru','25 8943261',2,NULL,NULL,NULL,NULL,NULL,NULL),(6,'olgerdovich',_binary '$2a$10$eg3YGSrw1yw82VCaoMYFd.P.gqu5UHvxD/4/qSbN7b81mMxXPc12q','Вячеслав','Ольгердович','Верещагин','slavaolgerd@gmail.com','29 6941205',2,1,90,'PHL',1,1,1),(7,'vasia',_binary '$2a$10$V0KsFRmhnenHts9Xh9AfjO5A0NOFIBbYVIESWaRS0Ban6T.OF707y','Василий','Дементьевич','Зубров','vaszub@mail.ru','29 4301934',2,1,59,'CHM',12,1,1),(8,'timey',_binary '$2a$10$V0KsFRmhnenHts9Xh9AfjO5A0NOFIBbYVIESWaRS0Ban6T.OF707y','Тимей','Подворович','Тимейский','podvtim@mail.ru','29 7670991',1,NULL,NULL,NULL,NULL,NULL,NULL),(13,'golub',_binary '$2a$10$eg3YGSrw1yw82VCaoMYFd.P.gqu5UHvxD/4/qSbN7b81mMxXPc12q','Иван','Вильгельмович','Голубев','vilgelmovich@gmail.com','29 6439015',2,0,73,'PHL',2,1,1),(41,'ggallin',_binary '$2a$10$QQU7w1IJXt6in9XnOKH7WevSk7BFnvHajSLK5HoagFMSYB21yDHm6','Евгений','Потапович','Уточкин','ggyeahall@mail.ru','29 5831707',2,NULL,81,'CHM',12,0,0),(42,'nerd23',_binary '$2a$10$V0KsFRmhnenHts9Xh9AfjO5A0NOFIBbYVIESWaRS0Ban6T.OF707y','Алексей','Пафнутьевич','Лосев','lospaf@gmail.com','29 7401274',2,NULL,NULL,'CHM',NULL,0,NULL),(43,'alla',_binary '$2a$10$46QHWJmOrZ6NFg1rfylqzOtsyoOwj63maiAFwcn.R0mcmlGDYAUwa','Алла','Андреевна','Суркова','surkova22@mail.ru','29 3334455',2,NULL,90,'BIO',9,1,1),(44,'qwerty123',_binary '$2a$10$ePc/n7EHfCUSWF4dbym5weH.gwHB495SLcOW/kXPSXppIPAflN6G2','Лариса','Геннадьевна','Шульцман','shngfdlk@gmail.com','29 4709417',2,NULL,78,'INF',13,1,1),(45,'asdfg45',_binary '$2a$10$N8iA/2vk1wWyaigUhATag.nWyMHqEbpDqn/0wJ04D46CwgK3ywX6m','Вероника','Адамовна','Рябинова','trhwlkn@mail.ru','33 6104739',2,NULL,76,'BIO',8,1,1),(46,'goodboy',_binary '$2a$10$1gPNVo/pF4wdrcMCxCD6rOzgn2imMORs4Iilces.edfbW0IEauRlq','Илья','Максимович','Бук','bookread3g@gmail.ru','29 7560901',2,NULL,81,'BIO',9,1,1),(47,'yfaila',_binary '$2a$10$QFodQhYdNHTInMTF4twgf.F6FmbYuOGtg4.GakU.G9aZtpnBXtDy.','Ифаила','Дмитриевна','Брыж','brydzh@mail.ru','29 5740956',2,NULL,67,'BIO',8,1,1),(48,'roodik',_binary '$2a$10$Glvowm9pfeThQphU6OS/dOhll48almtyZnUXD5HFQqhpHiMJnDyHO','Рудольф','Варлеевич','Струбидзе','strudcmkl@yandex.com','29 7890956',2,NULL,81,'BIO',9,1,1),(49,'kabuk',_binary '$2a$10$f6ho9tiYZd4QBgw15VMlkOqpy0i3r6TsnWW6Tv9aVVdNqc.CIUKDW','Кабук','Андреевич','Иванов','kabuk@mail.ru','29 6669056',2,NULL,NULL,'BIO',NULL,0,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-23  3:27:03
