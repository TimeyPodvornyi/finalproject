-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: selection_committee
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `faculty`
--

DROP TABLE IF EXISTS `faculty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `faculty` (
  `id` char(3) NOT NULL COMMENT 'Первичный ключ таблицы, представляющий собой аббревиатуру факультета.',
  `faculty_name` varchar(45) NOT NULL COMMENT 'Название факультета.',
  `recruitment_plan` int(10) unsigned NOT NULL COMMENT 'План приема (количество абитуриентов, которое планируется зачислить на факультет). Из всего количества поданных заявок на факультет система выбирает не превышающее данное значение количество абитуриентов с лучшими результатами по экзаменам и аттестату.',
  `first_subject_id` int(10) unsigned NOT NULL COMMENT 'Внешний ключ первого из трех предметов, по которым сдаются экзамены для участия в конкурсе на данном факультете.',
  `second_subject_id` int(10) unsigned NOT NULL COMMENT 'Внешний ключ второго из трех предметов, по которым сдаются экзамены для участия в конкурсе на данном факультете.',
  `third_subject_id` int(10) unsigned NOT NULL COMMENT 'Внешний ключ третьего из трех предметов, по которым сдаются экзамены для участия в конкурсе на данном факультете.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `abbreviation_UNIQUE` (`id`),
  UNIQUE KEY `faculty_name_UNIQUE` (`faculty_name`),
  KEY `first_subject_id_idx` (`first_subject_id`),
  KEY `second_subject_id_idx` (`second_subject_id`),
  KEY `third_subject_id_idx` (`third_subject_id`),
  CONSTRAINT `first_subject_id` FOREIGN KEY (`first_subject_id`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `second_subject_id` FOREIGN KEY (`second_subject_id`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `third_subject_id` FOREIGN KEY (`third_subject_id`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Таблица, содержащая информацию о факультете (название; план приема абитуриентов; предметы, по которым сдаются экзамены, необходимые для участия в конкурсе на данном факультете';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faculty`
--

LOCK TABLES `faculty` WRITE;
/*!40000 ALTER TABLE `faculty` DISABLE KEYS */;
INSERT INTO `faculty` VALUES ('BIO','Faculty of Biology',4,4,6,3),('CHM','Chemical Faculty',6,6,1,3),('INF','Faculty of Informatics',7,2,5,3),('PHL','Faculty of Philosophy',3,7,8,3),('PHS','Faculty of Physics',6,1,2,3);
/*!40000 ALTER TABLE `faculty` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-23  3:27:04
