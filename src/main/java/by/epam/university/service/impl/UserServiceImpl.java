package by.epam.university.service.impl;

import by.epam.university.dao.DAOFactory;
import by.epam.university.dao.UserDAO;
import by.epam.university.dao.connection.ConnectionProvider;
import by.epam.university.dao.exception.DAOException;
import by.epam.university.model.User;
import by.epam.university.service.UserService;
import by.epam.university.service.exception.ServiceException;
import by.epam.university.service.exception.ValidationException;
import by.epam.university.service.util.PasswordEncrypter;
import by.epam.university.service.validator.Validator;

import java.util.List;

/**
 * It is a class for working with user operations.
 */
public class UserServiceImpl implements UserService {

    /**
     * {@link DAOFactory} instance for getting objects of DAO classes.
     */
    private static DAOFactory daoFactory = DAOFactory.getInstance();

    /**
     * {@link UserDAO} instance.
     */
    private static final UserDAO USER_DAO = daoFactory.getUserDAO();

    /**
     * {@inheritDoc}
     */
    @Override
    public int register(final User user)
            throws ValidationException, ServiceException {

                if (!Validator.getInstance()
                        .validateRegistrationInputData(user)) {
            throw new ValidationException("User's data are invalid!");
        }

        try {
            if (USER_DAO.isLoginExists(user.getLogin())) {
                throw new ServiceException("Login already exists");
            }
            if (USER_DAO.isEmailAlreadyExists(user)) {
                throw new ValidationException("Email already exists");
            }

            String securePass
                    = PasswordEncrypter.getInstance()
                    .encryptPassword(user.getPassword());
            user.setPassword(securePass);

            return USER_DAO.addUser(user);

        } catch (DAOException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public User logIn(final String login,
                      final String password)
            throws ValidationException, ServiceException {

        if (!Validator.getInstance().validateLoginInputData(login, password)) {
            throw new ValidationException("User's data are invalid!");
        }

        try {
            User user = USER_DAO.getUserByLoginAndPassword(login, password);

            if (user == null) {
                throw new ServiceException("No such user exists");
            }

            return user;

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public User fillInPersonalInfo(final int id)
        throws ServiceException {

        try {
            return USER_DAO.getUserPersonalInfo(id);

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
        public void editPersonalInfo(final User user)
            throws ValidationException, ServiceException {

        if (!Validator.getInstance().validatePersonalUserData(user)) {
            throw new ValidationException("User's data are invalid!");
        }

        try {
            USER_DAO.setUserPersonalInfo(user);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> getUserList() throws ServiceException {

        try {
            return USER_DAO.getUsersWithAppl();

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> getApplicantsList(final String facultyId)
            throws ServiceException {
        try {
            return USER_DAO.getFacultyApplicants(facultyId);

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void distributeApplicants(final List<User> applicants)
            throws ServiceException {

        ConnectionProvider connectionProvider
                = ConnectionProvider.getInstance();

        try {
            connectionProvider.startTransaction();

            for (User applicant : applicants) {

                if (applicant.isEnlisted()) {
                    USER_DAO.markUserAsEnlisted(applicant.getId());
                } else {
                    USER_DAO.markUserAsNotEnlisted(applicant.getId());
                }
            }
            connectionProvider.commitTransaction();

        } catch (DAOException e) {
            connectionProvider.abortTransaction();
            throw new ServiceException(e);

        } finally {
            connectionProvider.endTransaction();
        }
    }
}
