package by.epam.university.service;

import by.epam.university.model.Faculty;
import by.epam.university.service.exception.ServiceException;

import java.util.List;

/**
 * Works with faculty operations.
 */
public interface FacultyService {

    /**
     * Gets the names of subjects that are submitted.
     * for admission to the faculty and specialities
     * of the faculty and put them into {@link Faculty} instance
     * @param id faculty id
     * @return {@link Faculty} instance containing all the necessary data
     * @throws ServiceException
     *             the service layer exception
     */
    Faculty fillFacultyInfo(String id) throws ServiceException;

    /**
     * Gets the number of submitted applications
     * for the faculty and put them into {@link Faculty} instance.
     * @param faculty {@link Faculty} instance with faculty id
     * @throws ServiceException
     *             the service layer exception
     */
    Faculty addSubmittedApplications(Faculty faculty)
            throws ServiceException;

    /**
     * Creates a list of all the faculties.
     * @return list of {@link Faculty} instances
     * @throws ServiceException
     *             the service layer exception
     */
    List<Faculty> createListOfFaculties() throws ServiceException;

    /**
     * Gets the recruitment plan of the faculty.
     * @param facultyId id of the faculty
     * @return recruitment plan
     * @throws ServiceException
     *             the service layer exception
     */
    int takeRecruitmentPlan(String facultyId) throws ServiceException;
}
