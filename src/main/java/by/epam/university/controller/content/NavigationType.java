package by.epam.university.controller.content;

/**
 * Enum of allowable navigation types.
 */
public enum NavigationType {
    FORWARD, REDIRECT;
}
