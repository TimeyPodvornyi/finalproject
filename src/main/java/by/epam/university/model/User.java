package by.epam.university.model;

/**
 * Entity-class containing user information.
 */
public class User {

    /**
     * User's id.
     */
    private int id;

    /**
     * User's login.
     */
    private String login;
    /**
     * User's password.
     */
    private String password;

    /**
     * User's name.
     */
    private String name;

    /**
     * User's patronymic.
     */
    private String patronymic;

    /**
     * User's surname.
     */
    private String surname;

    /**
     * User's email address.
     */
    private String email;

    /**
     * User's phone number.
     */
    private String phone;

    /**
     * User's role.
     */
    private Role role;

    /**
     * This variable shows is an entrant enlisted.
     */
    private Boolean isEnlisted;

    /**
     * This variable shows is user sent an application
     * for admission to the university.
     */
    private Boolean applicationSent;

    /**
     * This variable shows is user's application confirmed by administrator.
     */
    private Boolean applicationConfirmed;

    /**
     * Id of the faculty to which the user is registered.
     */
    private String facultyId;

    /**
     * The specialty for which the entrant is applying.
     */
    private Speciality speciality;

    /**
     * Certificate which stores entrant's grades.
     */
    private Certificate certificate;

    /**
     * The amount of all the user's grades.
     */
    private int gradeAmount;

    /**
     * Instantiates a new User instance.
     */
    public User() {
    }

    /**
     * Gets user's id.
     * @return id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets user's id.
     * @param userId user's id.
     */
    public void setId(final int userId) {
        id = userId;
    }

    /**
     * Gets user's login.
     * @return login.
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets user's login.
     * @param log user's login.
     */
    public void setLogin(final String log) {
        login = log;
    }

    /**
     * Gets user's password.
     * @return password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets user's password.
     * @param pWord is user's password.
     */
    public void setPassword(final String pWord) {
        password = pWord;
    }

    /**
     * Gets user's name.
     * @return user's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets user's name.
     * @param nm user's name.
     */
    public void setName(final String nm) {
        name = nm;
    }

    /**
     * Gets user's patronymic.
     * @return user's patronymic.
     */
    public String getPatronymic() {
        return patronymic;
    }

    /**
     * Sets user's patronymic.
     * @param midName user's patronymic.
     */
    public void setPatronymic(final String midName) {
        patronymic = midName;
    }

    /**
     * Gets user's surname.
     * @return user's surname.
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets user's surname.
     * @param sName user's surname.
     */
    public void setSurname(final String sName) {
        surname = sName;
    }

    /**
     * Gets user's email.
     * @return user's email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets user's email.
     * @param mail user's email.
     */
    public void setEmail(final String mail) {
        email = mail;
    }

    /**
     * Gets user's phone number.
     * @return user's phone number.
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets user's phone number.
     * @param tel user's phone number.
     */
    public void setPhone(final String tel) {
        phone = tel;
    }

    /**
     * Gets user's role.
     * @return user's role.
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets user's role.
     * @param rl user's role.
     */
    public void setRole(final Role rl) {
        role = rl;
    }

    /**
     * Gets {@code isEnlisted}.
     * @return boolean value of the variable.
     */
    public Boolean isEnlisted() {
        return isEnlisted;
    }

    /**
     * Sets {@code isEnlisted}.
     * @param enlisted input boolean value.
     */
    public void setEnlisted(final Boolean enlisted) {
        isEnlisted = enlisted;
    }

    /**
     * Gets {@code applicationSent}.
     * @return boolean value of the variable
     */
    public Boolean isApplicationSent() {
        return applicationSent;
    }

    /**
     * Sets {@code applicationSent}.
     * @param applSent input boolean value
     */
    public void setApplicationSent(final boolean applSent) {
        applicationSent = applSent;
    }

    /**
     * Gets {@code applicationConfirmed}.
     * @return boolean value of the variable
     */
    public Boolean getApplicationConfirmed() {
        return applicationConfirmed;
    }

    /**
     * Sets {@code applicationConfirmed}.
     * @param applConf input boolean value
     */
    public void setApplicationConfirmed(final Boolean applConf) {
        applicationConfirmed = applConf;
    }

    /**
     * Gets user's certificate.
     * @return certificate.
     */
    public Certificate getCertificate() {
        return certificate;
    }

    /**
     * Sets user's certificate.
     * @param cert certificate which contains user's grades.
     */
    public void setCertificate(final Certificate cert) {
        certificate = cert;
    }

    /**
     * Gets the id of the faculty where the entrant is registered.
     * @return facultyId.
     */
    public String getFacultyId() {
        return facultyId;
    }

    /**
     * Sets the id of the faculty.
     * @param flt id the faculty where the entrant is registered.
     */
    public void setFacultyId(final String flt) {
        facultyId = flt;
    }

    /**
     * Gets the speciality chosen by the entrant.
     * @return speciality.
     */
    public Speciality getSpeciality() {
        return speciality;
    }

    /**
     * Sets the speciality.
     * @param spec of the speciality chosen by the entrant.
     */
    public void setSpeciality(final Speciality spec) {
        speciality = spec;
    }

    /**
     * Gets the amount of all user's grades.
     * @return amount of grades
     */
    public int getGradeAmount() {
        return gradeAmount;
    }

    /**
     * Sets the amount of user's grades.
     * @param amount amount of grades
     */
    public void setGradeAmount(final int amount) {
        gradeAmount = amount;
    }
}
