package by.epam.university.model;

/**
 * Enum contains languages of the app.
 */
public enum Locale {
    /**
     * English and russian locale.
     */
    EN, RU
}
