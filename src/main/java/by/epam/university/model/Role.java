package by.epam.university.model;

/**
 * Enum of possible user roles.
 */
public enum Role {
    /**
     * Admin role.
     */
    ADMIN,
    /**
     * Entrant role.
     */
    ENTRANT,
    /**
     * Unknown role.
     */
    UNKNOWN
}
