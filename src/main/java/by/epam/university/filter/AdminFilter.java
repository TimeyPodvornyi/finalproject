package by.epam.university.filter;

import by.epam.university.command.constant.PathConstants;
import by.epam.university.command.constant.SessionConstants;
import by.epam.university.model.Role;
import by.epam.university.util.ConfigurationManager;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterConfig;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Filter prevents access to the pages accessible only by admins.
 */
@WebFilter(urlPatterns = { "/jsp/admin/*"})
public class AdminFilter implements Filter {

    /**
     * Main page path.
     */
    private static final String MAIN_PAGE
            = ConfigurationManager.getInstance()
            .getPath(PathConstants.MAIN_PAGE);

    /**
     * Path to the error page.
     */
    private static final String ERROR_PAGE
            = ConfigurationManager.getInstance().getPath(
            PathConstants.ERROR_PAGE);

    /**
     * Takes user's role from session, if it's not {@code ADMIN} redirects
     * to login page with message shown.
     * {@inheritDoc}
     */
    public void doFilter(final ServletRequest req,
                         final ServletResponse resp,
                         final FilterChain chain)
            throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        HttpSession session = request.getSession();

        if (session.getAttribute(SessionConstants.ROLE) != Role.ADMIN) {

            if (session.getAttribute(SessionConstants.LOGIN) == null) {
                response.sendRedirect(ERROR_PAGE);
            }
            response.sendRedirect(MAIN_PAGE);

        } else {
            chain.doFilter(request, response);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(final FilterConfig config)
            throws ServletException {
    }
}
