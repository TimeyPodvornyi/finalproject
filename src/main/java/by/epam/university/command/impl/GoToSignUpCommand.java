package by.epam.university.command.impl;

import by.epam.university.command.Command;
import by.epam.university.command.constant.PathConstants;
import by.epam.university.controller.content.NavigationType;
import by.epam.university.controller.content.RequestContent;
import by.epam.university.controller.content.RequestResult;
import by.epam.university.util.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The command for moving to the registration menu.
 */
public class GoToSignUpCommand implements Command {

    /**
     * {@link Logger} instance for logging.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(GoToSignUpCommand.class);

    /**
     * Path to registration page.
     */
    private static final String REGISTRATION
            = ConfigurationManager.getInstance()
            .getPath(PathConstants.REGISTRATION);


    /**
     * {@inheritDoc}
     */
    public RequestResult execute(final RequestContent requestContent) {
        LOGGER.debug("Redirecting to the registration page");
        return new RequestResult(NavigationType.REDIRECT, REGISTRATION);
    }
}
