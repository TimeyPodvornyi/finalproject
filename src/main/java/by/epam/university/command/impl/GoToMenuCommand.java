package by.epam.university.command.impl;

import by.epam.university.command.Command;
import by.epam.university.command.constant.PathConstants;
import by.epam.university.command.constant.SessionConstants;
import by.epam.university.controller.content.NavigationType;
import by.epam.university.controller.content.RequestContent;
import by.epam.university.controller.content.RequestResult;
import by.epam.university.model.Role;
import by.epam.university.util.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The command for moving to the entrant or admin menu.
 */
public class GoToMenuCommand implements Command {

    /**
     * {@link Logger} instance for logging.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(GoToMenuCommand.class);

    /**
     * Path to the entrant menu.
     */
    private static final String ENTRANT_MENU
            = ConfigurationManager.getInstance().getPath(
            PathConstants.ENTRANT);

    /**
     * Path to the admin menu.
     */
    private static final String ADMIN_MENU
            = ConfigurationManager.getInstance().getPath(
            PathConstants.ADMIN);

    /**
     * {@inheritDoc}
     */
    public RequestResult execute(final RequestContent requestContent) {

        String path = requestContent.getCurrentPage();
        LOGGER.debug("Moving to the menu");

        if (requestContent.getSessionAttribute(
                SessionConstants.ROLE) == Role.ENTRANT) {
            path = ENTRANT_MENU;
        }

        if (requestContent.getSessionAttribute(
                SessionConstants.ROLE) == Role.ADMIN) {
            path = ADMIN_MENU;
        }

        return new RequestResult(NavigationType.REDIRECT, path);
    }
}
