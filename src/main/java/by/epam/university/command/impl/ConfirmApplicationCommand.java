package by.epam.university.command.impl;

import by.epam.university.command.Command;
import by.epam.university.command.constant.PathConstants;
import by.epam.university.command.constant.RequestConstants;
import by.epam.university.controller.content.NavigationType;
import by.epam.university.controller.content.RequestContent;
import by.epam.university.controller.content.RequestResult;
import by.epam.university.service.ApplicationService;
import by.epam.university.service.ServiceFactory;
import by.epam.university.service.exception.ServiceException;
import by.epam.university.util.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Confirms entrant's application.
 */
public class ConfirmApplicationCommand implements Command {

    /**
     * {@link Logger} instance for logging.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(ConfirmApplicationCommand.class);

    /**
     * {@link ApplicationService} instance.
     */
    private static final ApplicationService APPLICATION_SERVICE
            = ServiceFactory.getInstance().getApplicationService();

    /**
     * A path to the target page.
     */
    private static final String SUCCESS_PAGE
            = ConfigurationManager.getInstance().getPath(
                    PathConstants.MANAGE_APPLICATIONS_RELOAD);

    /**
     * A path to the error page.
     */
    private static final String ERROR_PAGE
            = ConfigurationManager.getInstance()
            .getPath(PathConstants.ERROR_PAGE);

    /**
     * {@inheritDoc}
     */
    @Override
    public RequestResult execute(final RequestContent requestContent) {

        String path;

        int userId = Integer.parseInt(
                requestContent.getParameter(RequestConstants.USER_ID));

        try {
            APPLICATION_SERVICE.confirmApplication(userId);
            path = SUCCESS_PAGE;
        } catch (ServiceException e) {
            LOGGER.warn(e.getMessage());
            path = ERROR_PAGE;
        }
        return new RequestResult(NavigationType.REDIRECT, path);
    }
}
