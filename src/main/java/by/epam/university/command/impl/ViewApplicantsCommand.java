package by.epam.university.command.impl;

import by.epam.university.command.Command;
import by.epam.university.command.constant.PathConstants;
import by.epam.university.command.constant.RequestConstants;
import by.epam.university.controller.content.NavigationType;
import by.epam.university.controller.content.RequestContent;
import by.epam.university.controller.content.RequestResult;
import by.epam.university.model.User;
import by.epam.university.service.ServiceFactory;
import by.epam.university.service.UserService;
import by.epam.university.service.exception.ServiceException;
import by.epam.university.util.ConfigurationManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Views the certificates of the applicants for the faculty.
 */
public class ViewApplicantsCommand implements Command {

    /**
     * {@link Logger} instance for logging.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(ViewApplicantsCommand.class);

    /**
     * {@link UserService} instance.
     */
    private static final UserService USER_SERVICE
            = ServiceFactory.getInstance().getUserService();

    /**
     * Path to the target page.
     */
    private static final String SUCCESS_PAGE
            = ConfigurationManager.getInstance()
            .getPath(PathConstants.VIEW_APPLICANTS);

    /**
     * Path to the error page.
     */
    private static final String ERROR_PAGE
            = ConfigurationManager.getInstance().getPath(
            PathConstants.ERROR_PAGE);

    /**
     * {@inheritDoc}
     */
    public RequestResult execute(final RequestContent requestContent) {

        String path;

        LOGGER.log(Level.DEBUG, "Moving to the applicants list");

        try {
            String facultyId
                    = requestContent.getParameter(RequestConstants.FACULTY_ID);
            List<User> userList = USER_SERVICE.getApplicantsList(facultyId);

            requestContent.setAttribute(
                    RequestConstants.USER_LIST, userList);

            path = SUCCESS_PAGE;

        } catch (ServiceException e) {
            LOGGER.log(Level.WARN, e);
            path = ERROR_PAGE;
        }

        return new RequestResult(NavigationType.FORWARD, path);
    }
}
