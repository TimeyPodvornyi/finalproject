package by.epam.university.command.impl;

import by.epam.university.command.Command;
import by.epam.university.command.constant.SessionConstants;
import by.epam.university.controller.content.NavigationType;
import by.epam.university.controller.content.RequestContent;
import by.epam.university.controller.content.RequestResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Changes the language.
 */
public class ChangeLanguageCommand implements Command {


    /**
     * {@link Logger} instance for logging.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(ChangeLanguageCommand.class);

    /**
     {@inheritDoc}
     */
    @Override
    public RequestResult execute(final RequestContent requestContent) {

        LOGGER.debug("Changing locale");

        requestContent.setSessionAttribute(SessionConstants.LOCALE,
                requestContent.getParameter(SessionConstants.LOCALE));

        return new RequestResult(
                NavigationType.REDIRECT, requestContent.getCurrentPage());
    }
}
