package by.epam.university.command.impl;

import by.epam.university.command.Command;
import by.epam.university.command.constant.PathConstants;
import by.epam.university.command.constant.RequestConstants;
import by.epam.university.controller.content.NavigationType;
import by.epam.university.controller.content.RequestContent;
import by.epam.university.controller.content.RequestResult;
import by.epam.university.service.ApplicationService;
import by.epam.university.service.CertificateService;
import by.epam.university.service.ServiceFactory;
import by.epam.university.service.exception.ServiceException;
import by.epam.university.util.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Rejects entrant's application.
 */
public class RejectApplicationCommand implements Command {

    /**
     * {@link Logger} instance for logging.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(RejectApplicationCommand.class);

    /**
     * {@link ServiceFactory} instance.
     */
    private static final ServiceFactory SERVICE_FACTORY
            = ServiceFactory.getInstance();
    /**
     * {@link ApplicationService} instance.
     */
    private static final ApplicationService APPLICATION_SERVICE
            = SERVICE_FACTORY.getApplicationService();

    /**
     * {@link CertificateService} instance.
     */
    private static final CertificateService CERTIFICATE_SERVICE
            = SERVICE_FACTORY.getCertificateService();

    /**
     * A path to the target page.
     */
    private static final String SUCCESS_PAGE
            = ConfigurationManager.getInstance().getPath(
            PathConstants.MANAGE_APPLICATIONS_RELOAD);

    /**
     * A path to the error page.
     */
    private static final String ERROR_PAGE
            = ConfigurationManager.getInstance()
            .getPath(PathConstants.ERROR_PAGE);

    /**
     * {@inheritDoc}
     */
    @Override
    public RequestResult execute(final RequestContent requestContent) {

        String path;

        int userId = Integer.parseInt(
                requestContent.getParameter(RequestConstants.USER_ID));

        try {
            APPLICATION_SERVICE.rejectApplication(userId);
            path = SUCCESS_PAGE;

            CERTIFICATE_SERVICE.clearUserCertificate(userId);

        } catch (ServiceException e) {
            LOGGER.warn(e.getMessage());
            path = ERROR_PAGE;
        }

        return new RequestResult(NavigationType.FORWARD, path);
    }
}
