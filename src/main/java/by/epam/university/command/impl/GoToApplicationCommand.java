package by.epam.university.command.impl;

import by.epam.university.command.Command;
import by.epam.university.command.constant.PathConstants;
import by.epam.university.command.constant.RequestConstants;
import by.epam.university.command.constant.SessionConstants;
import by.epam.university.controller.content.NavigationType;
import by.epam.university.controller.content.RequestContent;
import by.epam.university.controller.content.RequestResult;
import by.epam.university.model.Faculty;
import by.epam.university.service.FacultyService;
import by.epam.university.service.ServiceFactory;
import by.epam.university.service.exception.ServiceException;
import by.epam.university.util.ConfigurationManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The command for moving to the application form.
 */
public class GoToApplicationCommand implements Command {

    /**
     * {@link Logger} instance for logging.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(GoToSignUpCommand.class);

    /**
     * Path to the target page.
     */
    private static final String SET_APPLICATION_DATA
            = ConfigurationManager.getInstance()
            .getPath(PathConstants.SET_APPLICATION_DATA);

    /**
     * Path to fail page.
     */
    private static final String FAIL_PAGE
            = ConfigurationManager.getInstance().getPath(
            PathConstants.ERROR_PAGE);

    /**
     * {@link FacultyService} instance.
     */
    private static final FacultyService FACULTY_SERVICE
            = ServiceFactory.getInstance().getFacultyService();

    /**
     * {@inheritDoc}
     */
    public RequestResult execute(final RequestContent requestContent) {

        String path;

        LOGGER.log(Level.DEBUG,
                "Moving to the application form menu");

        String facultyId = (String) requestContent.getSessionAttribute(
                SessionConstants.FACULTY_ID);

        try {
            Faculty faculty = FACULTY_SERVICE.fillFacultyInfo(facultyId);
            requestContent.setAttribute(RequestConstants.FACULTY, faculty);

            path = SET_APPLICATION_DATA;

        } catch (ServiceException e) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.log(Level.WARN, e.getMessage());
            }
            path = FAIL_PAGE;
        }

        return new RequestResult(NavigationType.FORWARD, path);
    }
}
