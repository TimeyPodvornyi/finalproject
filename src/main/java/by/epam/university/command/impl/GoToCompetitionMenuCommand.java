package by.epam.university.command.impl;

import by.epam.university.command.Command;
import by.epam.university.command.constant.PathConstants;
import by.epam.university.command.constant.RequestConstants;
import by.epam.university.controller.content.NavigationType;
import by.epam.university.controller.content.RequestContent;
import by.epam.university.controller.content.RequestResult;
import by.epam.university.model.Faculty;
import by.epam.university.service.FacultyService;
import by.epam.university.service.ServiceFactory;
import by.epam.university.service.exception.ServiceException;
import by.epam.university.util.ConfigurationManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Command for moving to the competition management menu.
 */
public class GoToCompetitionMenuCommand implements Command {

    /**
     * {@link Logger} instance for logging.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(GoToCompetitionMenuCommand.class);

    /**
     * {@link FacultyService} instance.
     */
    private static final FacultyService FACULTY_SERVICE
            = ServiceFactory.getInstance().getFacultyService();

    /**
     * Path to the target page.
     */
    private static final String SUCCESS_PAGE
            = ConfigurationManager.getInstance()
            .getPath(PathConstants.COMPETITION_MENU);

    /**
     * Path to fail page.
     */
    private static final String ERROR_PAGE
            = ConfigurationManager.getInstance().getPath(
            PathConstants.ERROR_PAGE);

    /**
     * {@inheritDoc}
     */
    public RequestResult execute(final RequestContent requestContent) {

        String path;

        try {
            List<Faculty> faculties = FACULTY_SERVICE.createListOfFaculties();

            for (Faculty faculty : faculties) {
                FACULTY_SERVICE.addSubmittedApplications(faculty);
            }

            requestContent.setAttribute(
                    RequestConstants.FACULTY_LIST, faculties);

            path = SUCCESS_PAGE;

        } catch (ServiceException e) {
            LOGGER.log(Level.WARN, e);
            path = ERROR_PAGE;
        }

        return new RequestResult(NavigationType.FORWARD, path);
    }
}
