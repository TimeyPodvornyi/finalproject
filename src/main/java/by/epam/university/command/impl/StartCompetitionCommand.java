package by.epam.university.command.impl;

import by.epam.university.command.Command;
import by.epam.university.command.constant.PathConstants;
import by.epam.university.command.constant.RequestConstants;
import by.epam.university.controller.content.NavigationType;
import by.epam.university.controller.content.RequestContent;
import by.epam.university.controller.content.RequestResult;
import by.epam.university.model.Certificate;
import by.epam.university.model.ExamGrade;
import by.epam.university.model.User;
import by.epam.university.service.CertificateService;
import by.epam.university.service.FacultyService;
import by.epam.university.service.ServiceFactory;
import by.epam.university.service.UserService;
import by.epam.university.service.exception.ServiceException;
import by.epam.university.util.ConfigurationManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Command start the competition on the selected faculty.
 */
public class StartCompetitionCommand implements Command {

    /**
     * {@link Logger} instance for logging.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(StartCompetitionCommand.class);

    /**
     * {@link ServiceFactory} instance.
     */
    private static final ServiceFactory SERVICE_FACTORY
            = ServiceFactory.getInstance();

    /**
     * {@link UserService} instance.
     */
    private static final UserService USER_SERVICE
            = SERVICE_FACTORY.getUserService();

    /**
     * {@link CertificateService} instance.
     */
    private static final CertificateService CERTIFICATE_SERVICE
            = SERVICE_FACTORY.getCertificateService();

    /**
     * {@link FacultyService} instance.
     */
    private static final FacultyService FACULTY_SERVICE
            = SERVICE_FACTORY.getFacultyService();

    /**
     * Path to the target page.
     */
    private static final String SUCCESS_PAGE
            = ConfigurationManager.getInstance()
            .getPath(PathConstants.COMPETITION_RESULT);

    /**
     * Path to the error page.
     */
    private static final String ERROR_PAGE
            = ConfigurationManager.getInstance().getPath(
            PathConstants.ERROR_PAGE);

    /**
     * {@inheritDoc}
     */
    public RequestResult execute(final RequestContent requestContent) {

        String path;
        LOGGER.debug("Competition starts!");

        List<User> applicants;
        List<User> winList = new ArrayList<>();
        List<User> loseList = new ArrayList<>();

        try {
            String facultyId
                    = requestContent.getParameter(RequestConstants.FACULTY_ID);
            applicants = USER_SERVICE.getApplicantsList(facultyId);

            if (applicants.size() < 1) {
                return new RequestResult(
                        NavigationType.REDIRECT,
                        requestContent.getCurrentPage());
            }

            for (User user : applicants) {
                Certificate certificate =
                        CERTIFICATE_SERVICE.getCertificate(user.getId());
                user.setGradeAmount(countGradeAmount(certificate));
                user.setEnlisted(false);
            }

            int recruitmentPlan
                    = FACULTY_SERVICE.takeRecruitmentPlan(facultyId);

            if (recruitmentPlan < applicants.size()) {
                winList = determineWinners(applicants, recruitmentPlan);
            } else {
                for (User user : applicants) {
                    user.setEnlisted(true);
                    winList.add(user);
                }
            }

            USER_SERVICE.distributeApplicants(applicants);

            for (User user : applicants) {
                if (!user.isEnlisted()) {
                    loseList.add(user);
                }
            }

            requestContent.setAttribute(RequestConstants.WINNERS_LIST, winList);
            requestContent.setAttribute(RequestConstants.LOSERS_LIST, loseList);

            path = SUCCESS_PAGE;

        } catch (ServiceException e) {
            LOGGER.log(Level.WARN, e);
            path = ERROR_PAGE;
        }

        LOGGER.debug("Competition completed");
        return new RequestResult(NavigationType.FORWARD, path);
    }

    /**
     * Method counts the amount of user's grades.
     * @param certificate {@link Certificate} instance that contains
     *                                       all the entrant's grade values.
     * @return the amount of user's grades
     */
    private int countGradeAmount(final Certificate certificate) {

        int sum = certificate.getSchoolGrade();
        ExamGrade[] examGrades = certificate.getExamGrades();

        for (ExamGrade exam : examGrades) {
            sum += exam.getGrade();
        }

        return sum;
    }

    /**
     * Determines the entrants who were enlisted by results
     * of the competition.
     * @param applicantList the list of entrants
     *                      participating in the competition
     * @param recruitmentPlan recruitment plan of the faculty
     * @return a list of winners
     */
    private List<User> determineWinners(final List<User> applicantList,
                                  final int recruitmentPlan) {

        List<User> winList = new ArrayList<>();

        for (int i = 0; i < recruitmentPlan; i++) {
            int maxValue = 0;
            int index = -1;

            for (int j = 0; j < applicantList.size(); j++) {
                User applicant = applicantList.get(j);

                if (applicant.getGradeAmount() > maxValue
                        && !applicant.isEnlisted()) {
                    maxValue = applicant.getGradeAmount();
                    index = j;
                }
            }
            applicantList.get(index).setEnlisted(true);
            winList.add(applicantList.get(index));
        }
        return winList;
    }
}
