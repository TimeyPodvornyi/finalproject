package by.epam.university.util;

import java.util.ResourceBundle;

/**
 * Helps to get messages from the bundles.
 */
public final class ConfigurationManager {

    /**
     * Configuration manager single instance.
     */
    private static final ConfigurationManager INSTANCE
            = new ConfigurationManager();

    /**
     * Path bundle.
     */
    private static final  ResourceBundle PATH
            = ResourceBundle.getBundle("path");

    /**
     * Prevents getting the instance of Configuration Manager
     * with a help of constructor.
     */
    private ConfigurationManager() {
    }

    /**
     * Gets the instance of ConfigureManager.
     * @return the instance.
     */
    public static ConfigurationManager getInstance() {
        return INSTANCE;
    }

    /**
     * Defines the page.
     *
     * @param key the key
     * @return the PATH
     *
     */
    public String getPath(final String key) {
        return PATH.getString(key);
    }

    /**
     * Gets the database parameters.
     *
     * @param key the key
     * @param dbProperties bundle with data base properties
     * @return the database parameters
     */
    public String getDatabaseParameters(final String dbProperties,
                                        final String key) {

        ResourceBundle databaseParameters
                = ResourceBundle.getBundle(dbProperties);
        return databaseParameters.getString(key);
    }
}
