<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/parts/jstl.jsp" %>
<%@ include file="/jsp/parts/main-style.jsp" %>

<fmt:message bundle="${loc}" key="local.main.sign-in" var="signIn"/>
<fmt:message bundle="${loc}" key="local.main.sign-up" var="signUp"/>
<fmt:message bundle="${loc}" key="local.main.greetings" var="greetings"/>
<fmt:message bundle="${loc}" key="local.menu.main.title" var="menu"/>

<fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
<fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>

<html>
<head>
    <title>Menu</title>
</head>
<body>

<a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
</a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a class="js-scroll-trigger"><c:out value="${menu}"/></a>
        </li>

        <li class="sidebar-nav-item">
            <a class="btn btn-theme1" href="/controller?command=sign_up">${signUp}</a>
        </li>

        <c:if test="${sessionScope.local == 'ru'}">
            <li class="sidebar-nav-item">
                <a class="btn btn-theme1" href="/controller?command=select_language&local=en">${en_button}</a>
            </li>
        </c:if>
        <c:if test="${sessionScope.local == 'en'}">
            <li class="sidebar-nav-item">
                <a class="btn btn-theme1" href="/controller?command=select_language&local=ru">${ru_button}</a>
            </li>
        </c:if>
    </ul>
</nav>

<section class="callout">
    <div class="container text-center">
        <h2 class="mx-auto mb-5">${greetings}</h2>
        <form method="post" action="/controller">
            <input type="hidden" name="command" value="sign in">
            <button class="btn btn-primary btn-xl" type="submit">${signIn}</button>
        </form>
    </div>
</section>

<script src="/static/jquery/jquery.min.js"></script>
<script src="/static/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="/static/jquery-easing/jquery.easing.min.js"></script>

<script src="/static/js/stylish-portfolio.min.js"></script>

</body>
</html>
