<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/parts/jstl.jsp" %>
<%@ include file="/jsp/parts/main-style.jsp" %>

<fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
<fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>

<fmt:message bundle="${loc}" key="local.faculty.physics" var="physicsFaculty"/>
<fmt:message bundle="${loc}" key="local.faculty.biology" var="biologyFaculty"/>
<fmt:message bundle="${loc}" key="local.faculty.biology" var="chemicalFaculty"/>
<fmt:message bundle="${loc}" key="local.faculty.philosophy" var="philosophyFaculty"/>
<fmt:message bundle="${loc}" key="local.faculty.informatics" var="informaticsFaculty"/>

<fmt:message bundle="${loc}" key="local.menu.entrant.message.application-sent" var="applSent"/>
<fmt:message bundle="${loc}" key="local.menu.entrant.message.enlisted" var="enlisted"/>
<fmt:message bundle="${loc}" key="local.menu.entrant.message.not-enlisted" var="notEnlisted"/>
<fmt:message bundle="${loc}" key="local.menu.entrant.title" var="entrantTitle"/>
<fmt:message bundle="${loc}" key="local.menu.entrant.fill-out-application" var="fillAppl"/>
<fmt:message bundle="${loc}" key="local.menu.entrant.personal-info" var="personalInfo"/>
<fmt:message bundle="${loc}" key="local.logout.button" var="logout"/>
<fmt:message bundle="${loc}" key="local.menu.entrant.message.sent-application" var="sentAppl"/>
<fmt:message bundle="${loc}" key="local.menu.entrant.message.application-rejected" var="applRejected"/>
<fmt:message bundle="${loc}" key="local.menu.entrant.message.application-confirmed" var="applConfirmed"/>

<html>
<head>
    <title>ENTRANT MENU</title>
</head>

<body>

<a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
</a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a class="js-scroll-trigger"><c:out value="${sessionScope.login}"/></a>
        </li>

        <c:if test="${sessionScope.local == 'ru'}">
            <li class="sidebar-nav-item">
                <a class="btn btn-theme1" href="/controller?command=select_language&local=en">${en_button}</a>
            </li>
        </c:if>
        <c:if test="${sessionScope.local == 'en'}">
            <li class="sidebar-nav-item">
                <a class="btn btn-theme1" href="/controller?command=select_language&local=ru">${ru_button}</a>
            </li>
        </c:if>

        <c:if test="${sessionScope.enlisted == null}">
            <c:if test="${sessionScope.applicationSent == false}">
                <li class="sidebar-nav-item">
                    <a class="js-scroll-trigger" href="/controller?command=go_to_application">${fillAppl}</a>
                </li>
            </c:if>
        </c:if>

        <c:if test="${sessionScope.applicationSent == false}">
            <li class="sidebar-nav-item">
                <a class="js-scroll-trigger" href="/controller?command=personal_info">${personalInfo}</a>
            </li>
        </c:if>

        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="/controller?command=logout">${logout}</a>
        </li>

    </ul>
</nav>

<header class="masthead d-flex">
    <div class="container text-center my-auto">
        <c:choose>
            <c:when test="${sessionScope.facultyId eq 'PHS'}">
                <h1 class="mb-2"><c:out value="${physicsFaculty}"/></h1>
            </c:when>
            <c:when test="${sessionScope.facultyId eq 'BIO'}">
                <h1><c:out value="${biologyFaculty}"/></h1>
            </c:when>
            <c:when test="${sessionScope.facultyId eq 'CHM'}">
                <h1><c:out value="${chemicalFaculty}"/></h1>
            </c:when>
            <c:when test="${sessionScope.facultyId eq 'PHL'}">
                <h1><c:out value="${philosophyFaculty}"/></h1>
            </c:when>
            <c:when test="${sessionScope.facultyId eq 'INF'}">
                <h1><c:out value="${informaticsFaculty}"/></h1>
            </c:when>
        </c:choose>
        <h2 class="mb-1"><c:out value="${entrantTitle}"/></h2><br>
        <h3 class="mb-5">
            <em>
                <c:if test="${sessionScope.enlisted == true}">
                    <c:out value="${enlisted}"/>
                </c:if>

                <c:if test="${sessionScope.enlisted == false}">
                    <c:out value="${notEnlisted}"/>
                </c:if>

                <c:if test="${sessionScope.enlisted == null}">

                    <c:if test="${sessionScope.applicationSent == false}">
                        <c:if test="${sessionScope.applicationConfirmed == false}">
                            <c:out value="${applRejected}"/>
                        </c:if>
                        <c:if test="${sessionScope.applicationConfirmed == null}">
                            <c:out value="${sentAppl}"/>
                        </c:if>
                    </c:if>

                    <c:if test="${sessionScope.applicationSent == true}">
                        <c:if test="${sessionScope.applicationConfirmed == true}">
                            <c:out value="${applConfirmed}"/>
                        </c:if>

                        <c:if test="${sessionScope.applicationConfirmed == false}">
                            <c:out value="${applSent}"/>
                        </c:if>
                </c:if>
                </c:if>
            </em>
        </h3>
    </div>
    <div class="overlay"></div>
</header>

<script src="/static/jquery/jquery.min.js"></script>
<script src="/static/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="/static/jquery-easing/jquery.easing.min.js"></script>

<script src="/static/js/stylish-portfolio.min.js"></script>

</body>
</html>