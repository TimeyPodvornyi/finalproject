<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<%@ include file="/jsp/parts/jstl.jsp" %>
<%@ include file="/jsp/parts/personal-info-style.jsp" %>

<fmt:message bundle="${loc}" key="local.reg.field.name.rule" var="regNameRule"/>
<fmt:message bundle="${loc}" key="local.reg.field.phone.rule" var="regPhoneRule"/>
<fmt:message bundle="${loc}" key="local.edit-user-info.button.submit" var="editUserButtonSubmit"/>
<fmt:message bundle="${loc}" key="local.edit-user-info.title" var="title"/>

<fmt:message bundle="${loc}" key="local.user.name" var="name"/>
<fmt:message bundle="${loc}" key="local.user.patronymic" var="patronymic"/>
<fmt:message bundle="${loc}" key="local.user.surname" var="surname"/>
<fmt:message bundle="${loc}" key="local.user.email" var="email"/>
<fmt:message bundle="${loc}" key="local.user.phone" var="phone"/>

<html>
<head>
    <title>Personal Information</title>
</head>
<body>

<div class="container">
    <div class="row main-form">
        <form class="" method="post" action="/controller">

            <input type="hidden" name="command" value="edit_personal_info"/>

            <div class="form-group">
                <label for="name" class="cols-sm-2 control-label">
                    <c:out value="${name}"/>
                </label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="name"
                               id="name" placeholder="${regNameRule}"
                               pattern="[a-zA-Zа-яА-Я]{2,20}"
                               value="${requestScope.user.name}" required="required"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="cols-sm-2 control-label">
                    <c:out value="${patronymic}"/>
                </label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="patronymic"
                               id="patronymic" placeholder="${regNameRule}"
                               pattern="[a-zA-Zа-яА-Я]{2,20}"
                               value="${requestScope.user.patronymic}"
                               required="required"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="surname" class="cols-sm-2 control-label">
                    <c:out value="${surname}"/>
                </label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="surname"
                               id="surname" placeholder="${regNameRule}"
                               pattern="[a-zA-Zа-яА-Я]{2,20}"
                               value="${requestScope.user.surname}"
                               required="required"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="cols-sm-2 control-label">
                    <c:out value="${email}"/>
                </label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="email"
                               id="email" placeholder="Enter your Email"
                               pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}"
                               value="${requestScope.user.email}"
                               required="required"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="phone" class="cols-sm-2 control-label">
                    <c:out value="${phone}"/>
                </label>
                <div class="cols-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                        <input type="text" class="form-control" name="phone"
                               id="phone" placeholder="${regPhoneRule}"
                               pattern="^[0-9]{2,2}\s[0-9]{7,7}$"
                               value="${requestScope.user.phone}"
                               required="required"/>
                    </div>
                </div>
            </div>

            <div class="form-group ">
                <input class="btn btn-primary btn-lg btn-block login-button"
                       type="submit" value="${editUserButtonSubmit}"
                       required="required">
            </div>
        </form>
    </div>
</div>
</body>
</html>
