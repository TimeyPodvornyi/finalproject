<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="/jsp/parts/jstl.jsp" %>
<%@ include file="/jsp/parts/reg-form-style.jsp" %>

<fmt:message bundle="${loc}" key="local.appl.title" var="title"/>
<fmt:message bundle="${loc}" key="local.appl.field.school-grade" var="schoolGrade"/>
<fmt:message bundle="${loc}" key="local.appl.field.school-grade.rule" var="schoolGradeRule"/>
<fmt:message bundle="${loc}" key="local.appl.field.exams" var="examGrades"/>
<fmt:message bundle="${loc}" key="local.appl.field.exam-grade.rule" var="examGradeRule"/>
<fmt:message bundle="${loc}" key="local.appl.button.submit" var="sendApplication"/>
<fmt:message bundle="${loc}" key="local.appl.field.speciality" var="specialityField"/>

<fmt:message bundle="${loc}" key="local.subject.physics" var="physics"/>
<fmt:message bundle="${loc}" key="local.subject.mathematics" var="mathematics"/>
<fmt:message bundle="${loc}" key="local.subject.russian" var="russian"/>
<fmt:message bundle="${loc}" key="local.subject.biology" var="biology"/>
<fmt:message bundle="${loc}" key="local.subject.english" var="english"/>
<fmt:message bundle="${loc}" key="local.subject.chemistry" var="chemistry"/>
<fmt:message bundle="${loc}" key="local.subject.social-science" var="socialScience"/>
<fmt:message bundle="${loc}" key="local.subject.history" var="history"/>

<fmt:message bundle="${loc}" key="local.speciality.metaphisics-and-ontology" var="metaphisics"/>
<fmt:message bundle="${loc}" key="local.speciality.social-philosophy" var="socialPhilosophy"/>
<fmt:message bundle="${loc}" key="local.speciality.philosophy-of-science" var="philosophyOfScience"/>
<fmt:message bundle="${loc}" key="local.speciality.computer-physics" var="computerPhysics"/>
<fmt:message bundle="${loc}" key="local.speciality.physics-of-nanomaterials-and-nanotechnologies" var="nanomaterials"/>
<fmt:message bundle="${loc}" key="local.speciality.nuclear-physics" var="nuclearPhysics"/>
<fmt:message bundle="${loc}" key="local.speciality.biochemistry" var="biochemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.microbiology" var="microbiology"/>
<fmt:message bundle="${loc}" key="local.speciality.bioecology" var="bioecology"/>
<fmt:message bundle="${loc}" key="local.speciality.fundamental-chemistry" var="fundamentalChemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.high-energy-chemistry" var="highEnergyChemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.pharmaceutical-chemistry" var="pharmaceuticalChemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.computer-security" var="computerSecurity"/>
<fmt:message bundle="${loc}" key="local.speciality.software-development" var="softwareDevelopment"/>
<fmt:message bundle="${loc}" key="local.speciality.infocommunication-technologies" var="infocommunication"/>

<html>
<head>
    <title>Application</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 well">
            <h3 class="text-center"><c:out value="${title}"/></h3>

            <form method="post" action="/controller">
                <input type="hidden" name="command" value="send_application"/>

                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="${schoolGrade}"
                               name ="schoolCertificate" id="schoolCertificate" tabindex="1"
                               pattern="[0-9]{1,3}" title="${schoolGradeRule}"
                               required="required"/>
                    </div>
                </div>

                <h4 class="text-center"><c:out value="${examGrades}"/></h4>

                <div class="col-xs-12">
                    <div class="form-group">
                        <c:choose>
                        <c:when test="${requestScope.faculty.firstSubject eq 'PHYSICS'}">
                            <input type="text" class="form-control" placeholder="${physics}"
                                   name ="firstExamRange" id="firstExamRange" tabindex="2"
                                   pattern="[0-9]{1,3}" title="${examGradeRule}"
                                   required="required"/>
                        </c:when>
                        <c:when test="${requestScope.faculty.firstSubject eq 'MATHEMATICS'}">
                            <input type="text" class="form-control" placeholder="${mathematics}"
                                   name ="firstExamRange" id="firstExamRange" tabindex="2"
                                   pattern="[0-9]{1,3}" title="${examGradeRule}"
                                   required="required"/>
                        </c:when>
                        <c:when test="${requestScope.faculty.firstSubject eq 'RUSSIAN'}">
                            <input type="text" class="form-control" placeholder="${russian}"
                                   name ="firstExamRange" id="firstExamRange" tabindex="2"
                                   pattern="[0-9]{1,3}" title="${examGradeRule}"
                                   required="required"/>
                        </c:when>
                        <c:when test="${requestScope.faculty.firstSubject eq 'BIOLOGY'}">
                            <input type="text" class="form-control" placeholder="${biology}"
                                   name ="firstExamRange" id="firstExamRange" tabindex="2"
                                   pattern="[0-9]{1,3}" title="${examGradeRule}"
                                   required="required"/>
                        </c:when>
                        <c:when test="${requestScope.faculty.firstSubject eq 'ENGLISH'}">
                            <input type="text" class="form-control" placeholder="${english}"
                                   name ="firstExamRange" id="firstExamRange" tabindex="2"
                                   pattern="[0-9]{1,3}" title="${examGradeRule}"
                                   required="required"/>
                        </c:when>
                        <c:when test="${requestScope.faculty.firstSubject eq 'CHEMISTRY'}">
                            <input type="text" class="form-control" placeholder="${chemistry}"
                                   name ="firstExamRange" id="firstExamRange" tabindex="2"
                                   pattern="[0-9]{1,3}" title="${examGradeRule}"
                                   required="required"/>
                        </c:when>
                        <c:when test="${requestScope.faculty.firstSubject eq 'SOCIAL_SCIENCE'}">
                            <input type="text" class="form-control" placeholder="${socialScience}"
                                   name ="firstExamRange" id="firstExamRange" tabindex="2"
                                   pattern="[0-9]{1,3}" title="${examGradeRule}"
                                   required="required"/>
                        </c:when>
                        <c:when test="${requestScope.faculty.firstSubject eq 'HISTORY'}">
                            <input type="text" class="form-control" placeholder="${history}"
                                   name ="firstExamRange" id="firstExamRange" tabindex="2"
                                   pattern="[0-9]{1,3}" title="${examGradeRule}"
                                   required="required"/>
                        </c:when>
                        </c:choose>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${requestScope.faculty.secondSubject eq 'PHYSICS'}">
                                <input type="text" class="form-control" placeholder="${physics}"
                                       name ="secondExamRange" id="secondExamRange" tabindex="3"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.secondSubject eq 'MATHEMATICS'}">
                                <input type="text" class="form-control" placeholder="${mathematics}"
                                       name ="secondExamRange" id="secondExamRange" tabindex="3"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.secondSubject eq 'RUSSIAN'}">
                                <input type="text" class="form-control" placeholder="${russian}"
                                       name ="secondExamRange" id="secondExamRange" tabindex="3"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.secondSubject eq 'BIOLOGY'}">
                                <input type="text" class="form-control" placeholder="${biology}"
                                       name ="secondExamRange" id="secondExamRange" tabindex="3"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.secondSubject eq 'ENGLISH'}">
                                <input type="text" class="form-control" placeholder="${english}"
                                       name ="secondExamRange" id="secondExamRange" tabindex="3"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.secondSubject eq 'CHEMISTRY'}">
                                <input type="text" class="form-control" placeholder="${chemistry}"
                                       name ="secondExamRange" id="secondExamRange" tabindex="3"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.secondSubject eq 'SOCIAL_SCIENCE'}">
                                <input type="text" class="form-control" placeholder="${socialScience}"
                                       name ="secondExamRange" id="secondExamRange" tabindex="3"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.secondSubject eq 'HISTORY'}">
                                <input type="text" class="form-control" placeholder="${history}"
                                       name ="secondExamRange" id="secondExamRange" tabindex="3"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                        </c:choose>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${requestScope.faculty.thirdSubject eq 'PHYSICS'}">
                                <input type="text" class="form-control" placeholder="${physics}"
                                       name ="thirdExamRange" id="thirdExamRange" tabindex="4"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.thirdSubject eq 'MATHEMATICS'}">
                                <input type="text" class="form-control" placeholder="${mathematics}"
                                       name ="thirdExamRange" id="thirdExamRange" tabindex="4"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.thirdSubject eq 'RUSSIAN'}">
                                <input type="text" class="form-control" placeholder="${russian}"
                                       name ="thirdExamRange" id="thirdExamRange" tabindex="4"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.thirdSubject eq 'BIOLOGY'}">
                                <input type="text" class="form-control" placeholder="${biology}"
                                       name ="thirdExamRange" id="thirdExamRange" tabindex="4"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.thirdSubject eq 'ENGLISH'}">
                                <input type="text" class="form-control" placeholder="${english}"
                                       name ="thirdExamRange" id="thirdExamRange" tabindex="4"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.thirdSubject eq 'CHEMISTRY'}">
                                <input type="text" class="form-control" placeholder="${chemistry}"
                                       name ="thirdExamRange" id="thirdExamRange" tabindex="4"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.thirdSubject eq 'SOCIAL_SCIENCE'}">
                                <input type="text" class="form-control" placeholder="${socialScience}"
                                       name ="thirdExamRange" id="thirdExamRange" tabindex="4"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                            <c:when test="${requestScope.faculty.thirdSubject eq 'HISTORY'}">
                                <input type="text" class="form-control" placeholder="${history}"
                                       name ="thirdExamRange" id="thirdExamRange" tabindex="4"
                                       pattern="[0-9]{1,3}" title="${examGradeRule}"
                                       required="required"/>
                            </c:when>
                        </c:choose>
                    </div>
                </div>

                <h4 class="text-center"><c:out value="${specialityField}"/></h4>

                <select class="form-control selectpicker" required="required"
                name="speciality" title="Speciality">
                <c:forEach items="${requestScope.faculty.setOfSpecialities}" var="speciality">
                <option value="${speciality}">
                <c:choose>
                <c:when test="${speciality eq 'METAPHYSICS_AND_ONTOLOGY'}">
                <c:out value="${metaphisics}"/>
                </c:when>
                <c:when test="${speciality eq 'SOCIAL_PHILOSOPHY'}">
                <c:out value="${socialPhilosophy}"/>
                </c:when>
                <c:when test="${speciality eq 'PHILOSOPHY_OF_SCIENCE'}">
                <c:out value="${philosophyOfScience}"/>
                </c:when>
                <c:when test="${speciality eq 'COMPUTER_PHYSICS'}">
                <c:out value="${computerPhysics}"/>
                </c:when>
                <c:when test="${speciality eq 'PHYSICS_OF_NANOMATERIALS_AND_NANOTECHNOLOGIES'}">
                <c:out value="${nanomaterials}"/>
                </c:when>
                <c:when test="${speciality eq 'NUCLEAR_PHYSICS'}">
                <c:out value="${nuclearPhysics}"/>
                </c:when>
                <c:when test="${speciality eq 'BIOCHEMISTRY'}">
                <c:out value="${biochemistry}"/>
                </c:when>
                <c:when test="${speciality eq 'MICROBIOLOGY'}">
                <c:out value="${microbiology}"/>
                </c:when>
                <c:when test="${speciality eq 'BIOECOLOGY'}">
                <c:out value="${bioecology}"/>
                </c:when>
                <c:when test="${speciality eq 'FUNDAMENTAL_CHEMISTRY'}">
                <c:out value="${fundamentalChemistry}"/>
                </c:when>
                <c:when test="${speciality eq 'HIGH_ENERGY_CHEMISTRY'}">
                <c:out value="${highEnergyChemistry}"/>
                </c:when>
                <c:when test="${speciality eq 'PHARMACEUTICAL_CHEMISTRY'}">
                <c:out value="${pharmaceuticalChemistry}"/>
                </c:when>
                <c:when test="${speciality eq 'COMPUTER_SECURITY'}">
                <c:out value="${computerSecurity}"/>
                </c:when>
                <c:when test="${speciality eq 'SOFTWARE_DEVELOPMENT'}">
                <c:out value="${softwareDevelopment}"/>
                </c:when>
                <c:when test="${speciality eq 'INFOCOMMUNICATION_TECHNOLOGIES'}">
                <c:out value="${infocommunication}"/>
                </c:when>
                </c:choose>
                </option>
                </c:forEach>
                </select><br>

                <div class="text-center col-xs-12">
                    <input type="submit" class="btn btn-default" value="${sendApplication}" />
                </div>
            </form>
</body>
</html>
