<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="/static/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="/static/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <link href="/static/css/stylish-portfolio.min.css" rel="stylesheet">
</head>
<body>
</body>
