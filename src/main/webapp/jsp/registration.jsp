<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/parts/jstl.jsp" %>
<%@ include file="/jsp/parts/reg-form-style.jsp" %>

<fmt:message bundle="${loc}" key="local.reg.title" var="title"/>
<fmt:message bundle="${loc}" key="local.reg.field.login" var="login"/>
<fmt:message bundle="${loc}" key="local.reg.field.login.rule" var="loginRule"/>
<fmt:message bundle="${loc}" key="local.reg.field.password" var="password"/>
<fmt:message bundle="${loc}" key="local.reg.field.password.rule" var="passwordRule"/>
<fmt:message bundle="${loc}" key="local.reg.field.password.confirmation" var="passwordConfirm"/>
<fmt:message bundle="${loc}" key="local.reg.field.name" var="name"/>
<fmt:message bundle="${loc}" key="local.reg.field.name.rule" var="nameRule"/>
<fmt:message bundle="${loc}" key="local.reg.field.patronymic" var="patronymic"/>
<fmt:message bundle="${loc}" key="local.reg.field.surname" var="surname"/>
<fmt:message bundle="${loc}" key="local.reg.field.email" var="email"/>
<fmt:message bundle="${loc}" key="local.reg.field.email.rule" var="emailRule"/>
<fmt:message bundle="${loc}" key="local.reg.field.phone" var="phone"/>
<fmt:message bundle="${loc}" key="local.reg.field.phone.rule" var="phoneRule"/>
<fmt:message bundle="${loc}" key="local.reg.field.faculty" var="faculty"/>

<fmt:message bundle="${loc}" key="local.faculty.physics" var="physics"/>
<fmt:message bundle="${loc}" key="local.faculty.biology" var="biology"/>
<fmt:message bundle="${loc}" key="local.faculty.chemistry" var="chemistry"/>
<fmt:message bundle="${loc}" key="local.faculty.philosophy" var="philosophy"/>
<fmt:message bundle="${loc}" key="local.faculty.informatics" var="informatics"/>

<fmt:message bundle="${loc}" key="local.reg.button.submit" var="submit"/>

<html>
<head>
    <title>Registration</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 well">
            <h3 class="text-center"><c:out value="${title}"/></h3><br>
            <form method="post" action="/controller">
                <input type="hidden" name="command" value="registration"/>

                <h4 class="text-center"><c:out value="${login}"/></h4>
                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="text" class="form-control" required placeholder="${loginRule}"
                               name ="login" id="login" tabindex="1"/>
                    </div>
                </div>

                <h4 class="text-center"><c:out value="${password}"/></h4>
                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="password" class="form-control" required placeholder="${passwordRule}"
                               name ="password" id="password" tabindex="2"/>
                    </div>
                </div>

                <h4 class="text-center"><c:out value="${name}"/></h4>
                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="text" class="form-control" required placeholder="${nameRule}"
                               name ="name" id="name" tabindex="3"/>
                    </div>
                </div>

                <h4 class="text-center"><c:out value="${patronymic}"/></h4>
                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="text" class="form-control" required placeholder="${nameRule}"
                               name ="patronymic" id="patronymic" tabindex="4"/>
                    </div>
                </div>

                <h4 class="text-center"><c:out value="${surname}"/></h4>
                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="text" class="form-control" required placeholder="${nameRule}"
                               name ="surname" id="surname" tabindex="5"/>
                    </div>
                </div>

                <h4 class="text-center"><c:out value="${email}"/></h4>
                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="text" class="form-control" required placeholder="${emailRule}"
                               name ="email" id="email" tabindex="6"/>
                    </div>
                </div>

                <h4 class="text-center"><c:out value="${phone}"/></h4>
                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="text" class="form-control" required placeholder="${phoneRule}"
                               name ="phone" id="phone" tabindex="7"/>
                    </div>
                </div>

                <h4 class="text-center"><c:out value="${faculty}"/></h4>
                <select class="form-control" name="facultyId" title="Faculty" required="required">
                <option value="PHS">${physics}</option>
                <option value="BIO">${biology}</option>
                <option value="CHM">${chemistry}</option>
                <option value="PHL">${philosophy}</option>
                <option value="INF">${informatics}</option>
                </select><br>

                <div class="text-center col-xs-12">
                    <input type="submit" class="btn btn-default" value="${submit}" />
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
