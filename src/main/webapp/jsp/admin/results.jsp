<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/parts/jstl.jsp" %>
<%@ include file="/jsp/parts/main-style.jsp" %>

<fmt:message bundle="${loc}" key="local.button.go-to-menu" var="backToMenu"/>

<fmt:message bundle="${loc}" key="local.user.login" var="loginColumn"/>
<fmt:message bundle="${loc}" key="local.user.name" var="nameColumn"/>
<fmt:message bundle="${loc}" key="local.user.patronymic" var="patronymicColumn"/>
<fmt:message bundle="${loc}" key="local.user.surname" var="surnameColumn"/>
<fmt:message bundle="${loc}" key="local.user.email" var="emailColumn"/>
<fmt:message bundle="${loc}" key="local.user.phone" var="phoneColumn"/>
<fmt:message bundle="${loc}" key="local.user.speciality" var="specialityColumn"/>
<fmt:message bundle="${loc}" key="local.user.grade-amount" var="gradeAmount"/>

<fmt:message bundle="${loc}" key="local.speciality.metaphisics-and-ontology" var="metaphisics"/>
<fmt:message bundle="${loc}" key="local.speciality.social-philosophy" var="socialPhilosophy"/>
<fmt:message bundle="${loc}" key="local.speciality.philosophy-of-science" var="philosophyOfScience"/>
<fmt:message bundle="${loc}" key="local.speciality.computer-physics" var="computerPhysics"/>
<fmt:message bundle="${loc}" key="local.speciality.physics-of-nanomaterials-and-nanotechnologies" var="nanomaterials"/>
<fmt:message bundle="${loc}" key="local.speciality.nuclear-physics" var="nuclearPhysics"/>
<fmt:message bundle="${loc}" key="local.speciality.biochemistry" var="biochemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.microbiology" var="microbiology"/>
<fmt:message bundle="${loc}" key="local.speciality.bioecology" var="bioecology"/>
<fmt:message bundle="${loc}" key="local.speciality.fundamental-chemistry" var="fundamentalChemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.high-energy-chemistry" var="highEnergyChemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.pharmaceutical-chemistry" var="pharmaceuticalChemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.software-development" var="softwareDevelopment"/>
<fmt:message bundle="${loc}" key="local.speciality.infocommunication-technologies" var="infocommunication"/>
<fmt:message bundle="${loc}" key="local.competition.winners" var="winners"/>
<fmt:message bundle="${loc}" key="local.competition.losers" var="losers"/>

<html>
<head>
    <title>Competition results</title>
</head>
<body>

<div class="table-responsive">
    <table border="1" class="table user-list table-hover">
        <tr align="center">
            <th><c:out value="${winners}"/></th>
        </tr>
    </table>
</div>

    <table border="1" class="table user-list table-hover table-sm">
        <thead class="thead-dark">
        <tr align="center">
            <th><c:out value="${loginColumn}"/></th>
            <th><c:out value="${nameColumn}"/></th>
            <th><c:out value="${patronymicColumn}"/></th>
            <th><c:out value="${surnameColumn}"/></th>
            <th><c:out value="${emailColumn}"/></th>
            <th><c:out value="${phoneColumn}"/></th>
            <th><c:out value="${specialityColumn}"/></th>
            <th><c:out value="${gradeAmount}"/></th>
        </tr>
        </thead>
        <c:forEach items="${requestScope.winList}" var ="user">
            <tr align="center">
                <th><c:out value="${user.login}"/></th>
                <th><c:out value="${user.name}"/></th>
                <th><c:out value="${user.patronymic}"/></th>
                <th><c:out value="${user.surname}"/></th>
                <th><c:out value="${user.email}"/></th>
                <th><c:out value="375${user.phone}"/></th>
                <th>
                    <c:choose>
                        <c:when test="${user.speciality eq 'METAPHYSICS_AND_ONTOLOGY'}">
                            ${metaphisics}
                        </c:when>
                        <c:when test="${user.speciality eq 'SOCIAL_PHILOSOPHY'}">
                            ${socialPhilosophy}
                        </c:when>
                        <c:when test="${user.speciality eq 'PHILOSOPHY_OF_SCIENCE'}">
                            ${philosophyOfScience}
                        </c:when>
                        <c:when test="${user.speciality eq 'COMPUTER_PHYSICS'}">
                            ${computerPhysics}
                        </c:when>
                        <c:when test="${user.speciality eq 'PHYSICS_OF_NANOMATERIALS_AND_NANOTECHNOLOGIES'}">
                            ${nanomaterials}
                        </c:when>
                        <c:when test="${user.speciality eq 'NUCLEAR_PHYSICS'}">
                            ${nuclearPhysics}
                        </c:when>
                        <c:when test="${user.speciality eq 'BIOCHEMISTRY'}">
                            ${biochemistry}
                        </c:when>
                        <c:when test="${user.speciality eq 'MICROBIOLOGY'}">
                            ${microbiology}
                        </c:when>
                        <c:when test="${user.speciality eq 'BIOECOLOGY'}">
                            ${bioecology}
                        </c:when>
                        <c:when test="${user.speciality eq 'FUNDAMENTAL_CHEMISTRY'}">
                            ${fundamentalChemistry}
                        </c:when>
                        <c:when test="${user.speciality eq 'HIGH_ENERGY_CHEMISTRY'}">
                            ${highEnergyChemistry}
                        </c:when>
                        <c:when test="${user.speciality eq 'PHARMACEUTICAL_CHEMISTRY'}">
                            ${pharmaceuticalChemistry}
                        </c:when>
                        <c:when test="${user.speciality eq 'COMPUTER_SECURITY'}">
                            ${metaphisics}
                        </c:when>
                        <c:when test="${user.speciality eq 'SOFTWARE_DEVELOPMENT'}">
                            ${softwareDevelopment}
                        </c:when>
                        <c:when test="${user.speciality eq 'INFOCOMMUNICATION_TECHNOLOGIES'}">
                            ${infocommunication}
                        </c:when>
                    </c:choose>
                </th>
                <th>
                    <c:out value="${user.gradeAmount}"/>
                </th>
            </tr>
        </c:forEach>
    </table>

<div class="table-responsive">
    <table border="1" class="table user-list table-hover">
        <tr align="center">
            <th><c:out value="${losers}"/></th>
        </tr>
    </table>
</div>

<table border="1" class="table user-list table-hover table-sm">
    <thead class="thead-dark">
    <tr align="center">
        <th><c:out value="${loginColumn}"/></th>
        <th><c:out value="${nameColumn}"/></th>
        <th><c:out value="${patronymicColumn}"/></th>
        <th><c:out value="${surnameColumn}"/></th>
        <th><c:out value="${emailColumn}"/></th>
        <th><c:out value="${phoneColumn}"/></th>
        <th><c:out value="${specialityColumn}"/></th>
        <th><c:out value="${gradeAmount}"/></th>
    </tr>
    </thead>
    <c:forEach items="${requestScope.loseList}" var ="user">
        <tr align="center">
            <th><c:out value="${user.login}"/></th>
            <th><c:out value="${user.name}"/></th>
            <th><c:out value="${user.patronymic}"/></th>
            <th><c:out value="${user.surname}"/></th>
            <th><c:out value="${user.email}"/></th>
            <th><c:out value="375${user.phone}"/></th>
            <th>
                <c:choose>
                    <c:when test="${user.speciality eq 'METAPHYSICS_AND_ONTOLOGY'}">
                        ${metaphisics}
                    </c:when>
                    <c:when test="${user.speciality eq 'SOCIAL_PHILOSOPHY'}">
                        ${socialPhilosophy}
                    </c:when>
                    <c:when test="${user.speciality eq 'PHILOSOPHY_OF_SCIENCE'}">
                        ${philosophyOfScience}
                    </c:when>
                    <c:when test="${user.speciality eq 'COMPUTER_PHYSICS'}">
                        ${computerPhysics}
                    </c:when>
                    <c:when test="${user.speciality eq 'PHYSICS_OF_NANOMATERIALS_AND_NANOTECHNOLOGIES'}">
                        ${nanomaterials}
                    </c:when>
                    <c:when test="${user.speciality eq 'NUCLEAR_PHYSICS'}">
                        ${nuclearPhysics}
                    </c:when>
                    <c:when test="${user.speciality eq 'BIOCHEMISTRY'}">
                        ${biochemistry}
                    </c:when>
                    <c:when test="${user.speciality eq 'MICROBIOLOGY'}">
                        ${microbiology}
                    </c:when>
                    <c:when test="${user.speciality eq 'BIOECOLOGY'}">
                        ${bioecology}
                    </c:when>
                    <c:when test="${user.speciality eq 'FUNDAMENTAL_CHEMISTRY'}">
                        ${fundamentalChemistry}
                    </c:when>
                    <c:when test="${user.speciality eq 'HIGH_ENERGY_CHEMISTRY'}">
                        ${highEnergyChemistry}
                    </c:when>
                    <c:when test="${user.speciality eq 'PHARMACEUTICAL_CHEMISTRY'}">
                        ${pharmaceuticalChemistry}
                    </c:when>
                    <c:when test="${user.speciality eq 'COMPUTER_SECURITY'}">
                        ${metaphisics}
                    </c:when>
                    <c:when test="${user.speciality eq 'SOFTWARE_DEVELOPMENT'}">
                        ${softwareDevelopment}
                    </c:when>
                    <c:when test="${user.speciality eq 'INFOCOMMUNICATION_TECHNOLOGIES'}">
                        ${infocommunication}
                    </c:when>
                </c:choose>
            </th>
            <th>
                <c:out value="${user.gradeAmount}"/>
            </th>
        </tr>
    </c:forEach>
</table>

<form method="post" action="/controller">
    <input type="hidden" name="command" value="go_to_menu">
    <div class="container text-center">
        <input class="button" type="submit" value="${backToMenu}">
    </div>
</form>
</body>
</html>
