<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/parts/jstl.jsp" %>
<%@ include file="/jsp/parts/main-style.jsp" %>

<fmt:message bundle="${loc}" key="local.button.go-to-menu" var="backToMenu"/>
<fmt:message bundle="${loc}" key="local.competition.faculty.name" var="facultyNameColumn"/>
<fmt:message bundle="${loc}" key="local.competition.faculty.recruitment-plan" var="recruitmentPlanColumn"/>
<fmt:message bundle="${loc}" key="local.competition.faculty.applications" var="submittedApplicationsColumn"/>
<fmt:message bundle="${loc}" key="local.competition.button.view-applicants" var="viewApplicantsButton"/>
<fmt:message bundle="${loc}" key="local.competition.button.start" var="startButton"/>

<fmt:message bundle="${loc}" key="local.faculty.physics" var="physics"/>
<fmt:message bundle="${loc}" key="local.faculty.biology" var="biology"/>
<fmt:message bundle="${loc}" key="local.faculty.chemistry" var="chemistry"/>
<fmt:message bundle="${loc}" key="local.faculty.philosophy" var="philosophy"/>
<fmt:message bundle="${loc}" key="local.faculty.informatics" var="informatics"/>

<html>
<head>
    <title>Competition Menu</title>
</head>
<body>
<div class="table-responsive">
    <table border="1" class="table user-list table-hover table-sm">
        <thead class="thead-dark">
    <tr align="center">
        <th><c:out value="${facultyNameColumn}"/></th>
        <th><c:out value="${recruitmentPlanColumn}"/></th>
        <th><c:out value="${submittedApplicationsColumn}"/></th>
        <th></th><th></th>
    </tr>
        </thead>
<c:forEach items="${requestScope.facultyList}" var="faculty">
    <tr align="center">
        <th>
            <c:choose>
                <c:when test="${faculty.id eq 'PHS'}">
                        <c:out value="${physics}"/>
                </c:when>
                <c:when test="${faculty.id eq 'BIO'}">
                    <c:out value="${biology}"/>
                </c:when>
                <c:when test="${faculty.id eq 'CHM'}">
                    <c:out value="${chemistry}"/>
                </c:when>
                <c:when test="${faculty.id eq 'PHL'}">
                    <c:out value="${philosophy}"/>
                </c:when>
                <c:when test="${faculty.id eq 'INF'}">
                    <c:out value="${informatics}"/>
                </c:when>
            </c:choose>
        </th>
        <th><c:out value="${faculty.recruitmentPlan}"/></th>
        <th><c:out value="${faculty.submittedApplications}"/></th>
        <th>
            <a href="/controller?command=view_applicants&facultyId=${faculty.id}">
                <button type="submit">${viewApplicantsButton}</button>
            </a>
        </th>
        <th>
            <a href="/controller?command=start_competition&facultyId=${faculty.id}">
                <button>${startButton}</button>
            </a>
        </th>
    </tr>
    </c:forEach>
</table>
</div>

<form method="post" action="/controller">
    <input type="hidden" name="command" value="go_to_menu">
    <div class="container text-center">
        <input class="button" type="submit" value="${backToMenu}">
    </div>
</form>
</body>
</html>
