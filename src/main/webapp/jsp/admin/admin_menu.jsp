<%@ taglib prefix="ftm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/parts/jstl.jsp" %>
<%@ include file="/jsp/parts/main-style.jsp" %>

<fmt:message bundle="${loc}" key="local.locbutton.name.ru" var="ru_button"/>
<fmt:message bundle="${loc}" key="local.locbutton.name.en" var="en_button"/>

<ftm:message bundle="${loc}" key="local.menu.admin.title" var="adminTitle"/>
<ftm:message bundle="${loc}" key="local.menu.admin.certificate-verification" var="verification"/>
<ftm:message bundle="${loc}" key="local.menu.admin.competition-management" var="competition"/>
<ftm:message bundle="${loc}" key="local.logout.button" var="logout"/>

<html>
<head>
    <title>ADMIN</title>
</head>
<body>

<a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
</a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">

        <li class="sidebar-brand">
            <a class="js-scroll-trigger"><c:out value="${sessionScope.login}"/></a>
        </li>

        <c:if test="${sessionScope.local == 'ru'}">
            <li class="sidebar-nav-item">
                <a class="btn btn-theme1"
                   href="/controller?command=select_language&local=en">${en_button}</a>
            </li>
        </c:if>
        <c:if test="${sessionScope.local == 'en'}">
            <li class="sidebar-nav-item">
                <a class="btn btn-theme1"
                   href="/controller?command=select_language&local=ru">${ru_button}</a>
            </li>
        </c:if>

        <li class="sidebar-nav-item">
            <a class="btn btn-theme1"
               href="/controller?command=go_to_application_management">${verification}</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="btn btn-theme1"
               href="/controller?command=go_to_competition_management">${competition}</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="btn btn-theme1"
               href="/controller?command=logout">${logout}</a>
        </li>
    </ul>
</nav>

<header class="masthead d-flex">
    <div class="container text-center my-auto">
        <h1 class="mb-1">${adminTitle}</h1>
    </div>
</header>

<script src="/static/jquery/jquery.min.js"></script>
<script src="/static/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="/static/jquery-easing/jquery.easing.min.js"></script>

<script src="/static/js/stylish-portfolio.min.js"></script>

</body>
</html>
