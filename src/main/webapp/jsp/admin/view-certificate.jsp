<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/parts/jstl.jsp" %>
<%@ include file="/jsp/parts/main-style.jsp" %>

<fmt:message bundle="${loc}" key="local.subject.physics" var="physics"/>
<fmt:message bundle="${loc}" key="local.subject.mathematics" var="mathematics"/>
<fmt:message bundle="${loc}" key="local.subject.russian" var="russian"/>
<fmt:message bundle="${loc}" key="local.subject.biology" var="biology"/>
<fmt:message bundle="${loc}" key="local.subject.english" var="english"/>
<fmt:message bundle="${loc}" key="local.subject.chemistry" var="chemistry"/>
<fmt:message bundle="${loc}" key="local.subject.social-science" var="socialScience"/>
<fmt:message bundle="${loc}" key="local.subject.history" var="history"/>

<fmt:message bundle="${loc}" key="local.certificate.school-grade" var="schoolGrade"/>
<fmt:message bundle="${loc}" key="local.certificate.exam-grades" var="examGrades"/>

<html>
<head>
    <title>Certificate</title>
</head>
<body>
<table class="table">
    <thead class="thead-dark">
    <tr align="left">
        <th>
        <c:out value="${requestScope.user.surname} ${requestScope.user.name} ${requestScope.user.patronymic}"/>
        </th>
        <th></th>
    </tr>
    </thead>

    <tr class="table-primary">
        <th><c:out value="${examGrades}"/></th>
        <th></th>
    </tr>

    <c:forEach items="${requestScope.certificate.examGrades}" var="exam">
        <tr class="table-default">
            <th>
                <c:choose>
                    <c:when test="${exam.subject eq 'PHYSICS'}">
                        ${physics}
                    </c:when>
                    <c:when test="${exam.subject eq 'MATHEMATICS'}">
                        ${mathematics}
                    </c:when>
                    <c:when test="${exam.subject eq 'RUSSIAN'}">
                        ${russian}
                    </c:when>
                    <c:when test="${exam.subject eq 'BIOLOGY'}">
                        ${biology}
                    </c:when>
                    <c:when test="${exam.subject eq 'ENGLISH'}">
                        ${english}
                    </c:when>
                    <c:when test="${exam.subject eq 'CHEMISTRY'}">
                        ${chemistry}
                    </c:when>
                    <c:when test="${exam.subject eq 'SOCIAL_SCIENCE'}">
                        ${socialScience}
                    </c:when>
                    <c:when test="${exam.subject eq 'HISTORY'}">
                        ${history}
                    </c:when>
                </c:choose>
            </th>
            <th align="center"><c:out value="${exam.grade}"/></th>
        </tr>
    </c:forEach>

    <tr class="table-primary">
        <th>
            <c:out value="${schoolGrade}"/>
        </th>
        <th align="center">
            <c:out value="${requestScope.certificate.schoolGrade}"/>
        </th>
    </tr>
</table>
</body>
</html>
