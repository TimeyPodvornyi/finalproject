<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/parts/jstl.jsp" %>
<%@ include file="/jsp/parts/main-style.jsp" %>

<fmt:message bundle="${loc}" key="local.appl-manage.button.certificate" var="certButton"/>
<fmt:message bundle="${loc}" key="local.appl-manage.button.confirm" var="confirmButton"/>
<fmt:message bundle="${loc}" key="local.appl-manage.button.reject" var="rejectButton"/>
<fmt:message bundle="${loc}" key="local.button.go-to-menu" var="backToMenu"/>

<fmt:message bundle="${loc}" key="local.user.login" var="loginColumn"/>
<fmt:message bundle="${loc}" key="local.user.name" var="nameColumn"/>
<fmt:message bundle="${loc}" key="local.user.patronymic" var="patronymicColumn"/>
<fmt:message bundle="${loc}" key="local.user.surname" var="surnameColumn"/>
<fmt:message bundle="${loc}" key="local.user.email" var="emailColumn"/>
<fmt:message bundle="${loc}" key="local.user.phone" var="phoneColumn"/>
<fmt:message bundle="${loc}" key="local.user.faculty" var="facultyColumn"/>
<fmt:message bundle="${loc}" key="local.user.speciality" var="specialityColumn"/>
<fmt:message bundle="${loc}" key="local.user.status" var="statusColumn"/>
<fmt:message bundle="${loc}" key="local.appl-manage.confirmed" var="confirmed"/>

<fmt:message bundle="${loc}" key="local.speciality.metaphisics-and-ontology" var="metaphisics"/>
<fmt:message bundle="${loc}" key="local.speciality.social-philosophy" var="socialPhilosophy"/>
<fmt:message bundle="${loc}" key="local.speciality.philosophy-of-science" var="philosophyOfScience"/>
<fmt:message bundle="${loc}" key="local.speciality.computer-physics" var="computerPhysics"/>
<fmt:message bundle="${loc}" key="local.speciality.physics-of-nanomaterials-and-nanotechnologies" var="nanomaterials"/>
<fmt:message bundle="${loc}" key="local.speciality.nuclear-physics" var="nuclearPhysics"/>
<fmt:message bundle="${loc}" key="local.speciality.biochemistry" var="biochemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.microbiology" var="microbiology"/>
<fmt:message bundle="${loc}" key="local.speciality.bioecology" var="bioecology"/>
<fmt:message bundle="${loc}" key="local.speciality.fundamental-chemistry" var="fundamentalChemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.high-energy-chemistry" var="highEnergyChemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.pharmaceutical-chemistry" var="pharmaceuticalChemistry"/>
<fmt:message bundle="${loc}" key="local.speciality.computer-security" var="computerSecurity"/>
<fmt:message bundle="${loc}" key="local.speciality.software-development" var="softwareDevelopment"/>
<fmt:message bundle="${loc}" key="local.speciality.infocommunication-technologies" var="infocommunication"/>

<html>
<head>
    <title>Entrant Applications</title>
</head>
<body>
<div class="table-responsive">
    <table border="1" class="table user-list table-hover table-sm">

        <thead class="thead-dark">
        <tr align="center">
            <th>${loginColumn}</th>
            <th>${nameColumn}</th>
            <th>${patronymicColumn}</th>
            <th>${surnameColumn}</th>
            <th>${emailColumn}</th>
            <th>${phoneColumn}</th>
            <th>${facultyColumn}</th>
            <th>${specialityColumn}</th>
            <th>${statusColumn}</th>
            <th></th>
    </tr>
        </thead>

    <c:forEach items="${requestScope.userList}" var ="user">
        <c:if test="${user.applicationConfirmed == true}">

        <tr class="table-success" align="center">
            <th><c:out value="${user.login}"/></th>
            <th><c:out value="${user.name}"/></th>
            <th><c:out value="${user.patronymic}"/></th>
            <th><c:out value="${user.surname}"/></th>
            <th><c:out value="${user.email}"/></th>
            <th><c:out value="375${user.phone}"/></th>
            <th><c:out value="${user.facultyId}"/></th>
            <th><c:choose>
                <c:when test="${user.speciality eq 'METAPHYSICS_AND_ONTOLOGY'}">
                    <c:out value="${metaphisics}"/>
                </c:when>
                <c:when test="${user.speciality eq 'SOCIAL_PHILOSOPHY'}">
                    <c:out value="${socialPhilosophy}"/>
                </c:when>
                <c:when test="${user.speciality eq 'PHILOSOPHY_OF_SCIENCE'}">
                    <c:out value="${philosophyOfScience}"/>
                </c:when>
                <c:when test="${user.speciality eq 'COMPUTER_PHYSICS'}">
                    <c:out value="${computerPhysics}"/>
                </c:when>
                <c:when test="${user.speciality eq 'PHYSICS_OF_NANOMATERIALS_AND_NANOTECHNOLOGIES'}">
                    <c:out value="${nanomaterials}"/>
                </c:when>
                <c:when test="${user.speciality eq 'NUCLEAR_PHYSICS'}">
                    <c:out value="${nuclearPhysics}"/>
                </c:when>
                <c:when test="${user.speciality eq 'BIOCHEMISTRY'}">
                    <c:out value="${biochemistry}"/>
                </c:when>
                <c:when test="${user.speciality eq 'MICROBIOLOGY'}">
                    <c:out value="${microbiology}"/>
                </c:when>
                <c:when test="${user.speciality eq 'BIOECOLOGY'}">
                    <c:out value="${bioecology}"/>
                </c:when>
                <c:when test="${user.speciality eq 'FUNDAMENTAL_CHEMISTRY'}">
                    <c:out value="${fundamentalChemistry}"/>
                </c:when>
                <c:when test="${user.speciality eq 'HIGH_ENERGY_CHEMISTRY'}">
                    <c:out value="${highEnergyChemistry}"/>
                </c:when>
                <c:when test="${user.speciality eq 'PHARMACEUTICAL_CHEMISTRY'}">
                    <c:out value="${pharmaceuticalChemistry}"/>
                </c:when>
                <c:when test="${user.speciality eq 'COMPUTER_SECURITY'}">
                    <c:out value="${metaphisics}"/>
                </c:when>
                <c:when test="${user.speciality eq 'SOFTWARE_DEVELOPMENT'}">
                    <c:out value="${softwareDevelopment}"/>
                </c:when>
                <c:when test="${user.speciality eq 'INFOCOMMUNICATION_TECHNOLOGIES'}">
                    <c:out value="${infocommunication}"/>
                </c:when>
            </c:choose></th>
            <th>
                <c:out value="${confirmed}"/>
            </th>

            <th>
                <a href="/controller?command=view_certificate&userId=${user.id}">
                    <button type="submit">${certButton}</button>
                </a>
            </th>

        </tr>
        </c:if>
        <c:if test="${user.applicationConfirmed == false}">
            <tr class="table-warning" align="center">

            <th><c:out value="${user.login}"/></th>
            <th><c:out value="${user.name}"/></th>
            <th><c:out value="${user.patronymic}"/></th>
            <th><c:out value="${user.surname}"/></th>
            <th><c:out value="${user.email}"/></th>
            <th><c:out value="375${user.phone}"/></th>
            <th><c:out value="${user.facultyId}"/></th>
            <th>
                <c:choose>
                    <c:when test="${user.speciality eq 'METAPHYSICS_AND_ONTOLOGY'}">
                        <c:out value="${metaphisics}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'SOCIAL_PHILOSOPHY'}">
                        <c:out value="${socialPhilosophy}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'PHILOSOPHY_OF_SCIENCE'}">
                        <c:out value="${philosophyOfScience}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'COMPUTER_PHYSICS'}">
                        <c:out value="${computerPhysics}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'PHYSICS_OF_NANOMATERIALS_AND_NANOTECHNOLOGIES'}">
                        <c:out value="${nanomaterials}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'NUCLEAR_PHYSICS'}">
                        <c:out value="${nuclearPhysics}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'BIOCHEMISTRY'}">
                        <c:out value="${biochemistry}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'MICROBIOLOGY'}">
                        <c:out value="${microbiology}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'BIOECOLOGY'}">
                        <c:out value="${bioecology}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'FUNDAMENTAL_CHEMISTRY'}">
                        <c:out value="${fundamentalChemistry}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'HIGH_ENERGY_CHEMISTRY'}">
                        <c:out value="${highEnergyChemistry}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'PHARMACEUTICAL_CHEMISTRY'}">
                        <c:out value="${pharmaceuticalChemistry}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'COMPUTER_SECURITY'}">
                        <c:out value="${computerSecurity}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'SOFTWARE_DEVELOPMENT'}">
                        <c:out value="${softwareDevelopment}"/>
                    </c:when>
                    <c:when test="${user.speciality eq 'INFOCOMMUNICATION_TECHNOLOGIES'}">
                        <c:out value="${infocommunication}"/>
                    </c:when>
                </c:choose>
            </th>
                <th>
                    <c:if test="${user.applicationConfirmed == false}">
                    <a href="/controller?command=confirm_application&userId=${user.id}">
                    <button type="submit">${confirmButton}</button>
                    </a>
                    <a href="/controller?command=reject_application&userId=${user.id}">
                    <button type="submit">${rejectButton}</button>
                    </a>
                    </c:if>
                </th>
                <th>
                    <a href="/controller?command=view_certificate&userId=${user.id}">
                        <button type="submit">${certButton}</button>
                    </a>
                </th>
            </tr>
        </c:if>
    </c:forEach>
</table>
        </div>
            <form method="post" action="/controller">
                <input type="hidden" name="command" value="go_to_menu">
                <div class="container text-center">
                <input class="button" type="submit" value="${backToMenu}">
                </div>
            </form>
</body>
</html>
